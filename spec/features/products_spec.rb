require 'rails_helper'

RSpec.feature "Products", type: :feature do
  describe "商品詳細ページに関するテスト" do
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:taxon) { create(:taxon) }

    before do
      visit potepan_product_path(product.id)
    end

    context "商品詳細ページで表示されている内容" do
      it "HOMEへのリンクがあること" do
        expect(page).to have_link "Home"
      end

      it "商品名が表示されていること" do
        expect(page).to have_content product.name
      end

      it "商品の値段が表示されていること" do
        expect(page).to have_content product.display_price
      end

      it "商品説明が表示されていること" do
        expect(page).to have_content product.description
      end

      it "カテゴリーページへ遷移すること" do
        click_link "一覧ページへ戻る"
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end
  end
end
