require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  describe "カテゴリーページに関するテスト" do
    let!(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon, parent: taxonomy.root, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }

    before do
      visit potepan_category_path(taxon.id)
    end

    it "カテゴリーページで表示されている内容" do
      expect(page).to have_content "商品カテゴリー"
      expect(page).to have_content "色から探す"
      expect(page).to have_content "サイズから探す"
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      within('.side-nav') do
        expect(page).to have_content taxon.name
        expect(page).to have_content taxonomy.name
        expect(page).to have_content taxon.products.count
      end
    end

    it "商品名をクリックしたとき商品詳細ページへ遷移すること" do
      click_on product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end

    it "商品価格をクリックしたとき商品詳細ページへ遷移すること" do
      click_on product.display_price
      expect(current_path).to eq potepan_product_path(product.id)
    end

    it "分類(taxon)をクリックしたときそのカテゴリーページへ遷移すること" do
      click_on taxon.name
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end
end
