require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title(page_title)" do
    context "ページタイトルがある場合" do
      it "full_titleが表示されていること" do
        expect(full_title("test")).to eq "test - BIGBAG Store"
      end
    end

    context "ページタイトルがない場合" do
      it "空白の場合BIG BAGのみを返すこと" do
        expect(full_title("")).to eq "BIGBAG Store"
      end
    end

    context "ページタイトルにnilが渡ってきた場合" do
      it "BIG BAGのみを返すこと" do
        expect(full_title(nil)).to eq "BIGBAG Store"
      end
    end
  end
end
