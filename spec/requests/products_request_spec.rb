require 'rails_helper'

RSpec.describe "Products_request", type: :request do
  describe "GET #show" do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon) }

    before do
      get potepan_product_path(product.id)
    end

    it "レスポンス成功を確認" do
      expect(response).to have_http_status 200
    end

    it "showテンプレートの描画を確認" do
      expect(response).to render_template :show
    end

    it "商品名が表示されていること" do
      expect(response.body).to include product.name
    end

    it "商品価格が表示されていること" do
      expect(response.body).to include product.display_price.to_s
    end

    it "商品説明が表示されていること" do
      expect(response.body).to include product.description
    end

    it "@productへの受け渡しを確認" do
      expect(assigns(:product)).to eq product
    end

    it "カテゴリーページへのリンクが存在するか確認" do
      expect(response.body).to include "一覧ページへ戻る"
      expect(response.body).to include potepan_category_path(product.taxons.first.id)
    end
  end
end
