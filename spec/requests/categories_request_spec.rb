require 'rails_helper'

RSpec.describe "Categories_request", type: :request do
  describe "GET #show" do
    let!(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon) }
    let(:taxonomy) { create(:taxonomy) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "商品名が表示されること" do
      expect(response.body).to include product.name
    end

    it "商品価格が表示されること" do
      expect(response.body).to include product.display_price.to_s
    end

    it "レスポンス成功を確認" do
      expect(response).to have_http_status 200
    end

    it "カテゴリ(taxonomy)が表示されること" do
      expect(response.body).to include taxonomy.name
      expect(response.body).to include 'Brand'
    end

    it "分類(taxon)が表示されること" do
      expect(response.body).to include taxon.name
    end
  end
end
